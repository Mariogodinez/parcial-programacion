package Promedio;

import rx.Observable;
import rx.observables.MathObservable;


public class PrecioMaximo {

    public static void main(String[] args) {

        Observable<Integer> numbers = Observable.just(300, 300, 200, 800, 230);
        MathObservable.max(numbers).subscribe(System.out::println);

    }

}
