package Promedio;

import dto.Dato;
import rx.Observable;
import rx.observables.MathObservable;

import java.util.ArrayList;
import java.util.List;

public class PromedioPrecios{
    public static void main(String[] args) {

        List<Dato> Datos = new ArrayList<>();
        Datos.add(new Dato(1, 300));
        Datos.add(new Dato(2, 300));
        Datos.add(new Dato(3, 200));
        Datos.add(new Dato(4, 800));
        Datos.add(new Dato(5, 230));


        Observable<Dato> datosObservable = Observable.from(Datos);

        MathObservable
                .from(datosObservable)
                //interceptor
                .averageInteger(Dato::getCantidad)
                .subscribe((promedio) -> {
                    System.out.println("PROMEDIO:" + promedio);
                });
    }
}