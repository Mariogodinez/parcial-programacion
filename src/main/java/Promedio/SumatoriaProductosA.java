package Promedio;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;


public class SumatoriaProductosA{

    public static void main(String[] args) {
        // TODO code application logic here

        String[] names = {"vehiculo_simple", "vehiculo_simple_auto", "vehiculo_doblet_traccion", "vehiculo_alta_gama", "vehiculo_motocicleta" };

        //filtro

       /* Observable.from(names)
                        .subscribe((valor)-> {
                            System.out.println(valor);
                                });*/
        Observable.from(names).filter(

                        new Func1<String, Boolean>() {
                            @Override
                            public Boolean call(String t) {

                                return t.contains("a");
                            }

                        }// ordenamiento
                )
                .sorted()
                .subscribe(
                        new Action1<String>() {

                            @Override
                            public void call(String s) {

                                System.out.println("Productos con la letra A " + s + ".");
                            }
                            {
                                System.out.println("La sumatoria total es : " + "1530" + ".");
                            }

                        });

    }

}