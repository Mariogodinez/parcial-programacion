package dto;

public class Dato{
    private int codigo;
    private int cantidad;

    public Dato() {
    }

    public Dato(int codigo, int cantidad) {
        this.codigo = codigo;
        this.cantidad = cantidad;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
